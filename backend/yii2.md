# Рекомендации по Yii2

## Создание проекта
1. Создаем проект с помощью `composer`. Перейдите в терминале в папку, где у вас хранятся ваши проекты и выполните команду:
```shell
# <myproject> - Название вашего проекта
composer create-project yii2-bridge/app <myproject>
```

2. Перейдите в терминале в папку проекта и выполните команду:
```shell
cp config/example.local.php config/local.php
```

3. Создайте базу данных. В файле `config/local.php` пропишите конфигурации БД:  
```php
$db = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2bridge_local', // dbname - Название БД
    'username' => 'root', // dbname - Имя пользователя
    'password' => '', // password - Пароль
    'charset' => 'utf8mb4',
];
```

4. Далее в терминале выполняем следующую команду для запуска миграции:
```shell
# Для macOS/Linux
./vendor/bin/bridge-install

# Для Windows
call /vendor/bin/bridge-install
```

5. Создаем первого нашего пользователя:
```shell
# <email> - Email (admin@admin.com)
# <username> - Логин (admin)
# <password> - Пароль, не менее 6-ти символов (123456)
# <role> - Роль (admin)
php yii user/create <email> <username> <password> <role>
```

Поздравляю! Вы создали ваш первый проект. В адресной строке браузера введите `http://myproject.test/admin` и вы попадете на страницу авторазации в административный панель вашего проекта.

---
## Именование

Основные правила именование прочитайте [здесь](./naming.md). Далее все правила будут касаться только для фреймворка **Yii2**.

Что | Правило | Стиль написания | Правильно | Не правильно
------------ | ------------ | ------------- | ------------- | -------------
Метод отношения hasOne | ед. ч. | camelCase | getAuthor | getAuthors, get_authors
Метод отношения hasMany | мн. ч. | camelCase | getComments | getComment, get_comments

---
## Рекомендации
___
**TODO!**

### Миграции

> Всегда заполняйте метод **safeDown()** в миграциях.
Также желательно проверить миграцию, выполнив `php yii migrate/up` после выполнить `php yii migrate/down`
#### НЕ ВСТАВЛЯТЬ ДАННЫЕ ЧЕРЕЗ МИГРАЦИИ
Миграции необходимы только для создания/редактирования структуры баз данных

#### Часто встречающиеся колонки:  
`'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата создания')` - Дата и время создания записи, дата и время хранится в формате "Y-m-d H:i:s"  

`'updated_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP()')->comment('Дата обновления')` - Дата и время обновления записи, дата и время в формате "Y-m-d H:i:s"  

`'is_active' => $this->tyniInteger()->defaultValue(0)->comment('Активность')` - Bridge автоматически сгенерирует переключатель активности  

`'is_deleted' => $this->tyniInteger()->defaultValue(0)->comment('Удаленный'),` - Bridge "удаленные" записи будут хранится в корзине 

`'position' => $this->integer()->defaultValue(1)->comment('Позиция'),` - Bridge автоматически сгенерирует 4 кнопки для управления позицией записи

___
### Модели

**TODO!**

Модели создаем через Gii  

Модели называются в **единственном числе** - пример ``Post(), Card()``  

Порядок атрибутов и методов внутри модели
1. Константы - Название констант пишется **ЗАГЛАВНЫМИ** буквами, чаще всего состоит из двух частей ``TYPE_CONST``
2. Переменные  - Всегда ставится модификатор доступности (public/private/protected), тип переменной обязаиельно описывается в документации, название в camelCase ('public $typeExample')
3. tableName() -  название таблицы
4. rules() - правила валидации
5. Кастомные функции валидации
6. attributeLabels
7. behaviours
8. beforeSave, beforeDelete, beforeValidate
9. afterFind - **Не вешать запросы в эту функцию**
10. afterSave, afterDelete, afterValidate, afterRefresh
11. Relations
12. Свои функции

QueryModels
При создании модели через Gii  
![modelQuery](./images/ModelQuery.png)  
Обязательно генерируем Query модель и кладем ее в папку /query  
В QueryModel добавляем функции для ActiveQuery, чтобы не повторяться в вызовах модели 
```php
 /**
 * Возвращает активные объекты
 * @return ModelQuery
 */
 public function active()
    {
        return $this->andWhere(['is_active' => 1])->andWhere(['is_deleted' => 0]);
    }
```

