# Рекомендации по Laravel

## Создание проекта
**TODO!**

---
## Именование
Основные правила именование прочитайте [здесь](./naming.md). Далее все правила будут касаться только для фреймворка **Laravel**.

Что | Правило | Стиль написания | Правильно | Не правильно
------------ | ------------ | ------------- | ------------- | -------------
Метод отношения hasOne и belongsTo | ед. ч. | camelCase | articleComment | articleComments, article_comment
Все остальные отношения | мн. ч. | camelCase | articleComments | articleComment, article_comments
Метод в контроллере ресурсов | [таблица](https://laravel.com/docs/master/controllers#resource-controllers) | - | store | saveArticle
Индексы в конфиге и языковых файлах | - | snake_case | articles_enabled | ArticlesEnabled; articles-enabled
Представление | - | snake_case | show_filtered.blade.php | showFiltered.blade.php, show-filtered.blade.php~~
Конфигурационный файл | - | snake_case | google_calendar.php | googleCalendar.php, google-calendar.php
Имена маршрутов | - | snake_case | users.show_active | users.show-active, show-active-users
