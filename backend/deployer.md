# Настройка деплоя (Deployer)

## Установка
Подробнее об установке написано [здесь](./development-environment.md#deployer)

## Настройка конфигурации
### Yii2 && Laravel
```php
set('repository', 'git@gitlab.com:rocketfirm/myproject.git'); // Адрес git репозитория проекта

$userProduction = getenv('USER_PRODUCTION') ?: 'makers.kz'; // Имя пользователя от боевого сервера

$userStage = getenv('USER_STAGE') ?: 'rocketman'; // Имя пользователя от тестового сервера

host('makers.kz') // Адрес боевого сервера
    ->stage('prod')
    ->user($userProduction)
    ->set('http_user', $userProduction)
    ->set('http_group', 'pascln') // Группа пользователя, которую вы указали в $userProduction
    ->set('deploy_path', '~/myproject.kz'); // Путь к папке проекта
    
host('rocketfirm.net') // Адрес тестового сервера
    ->stage('stage')
    ->user($userStage)
    ->set('http_user', $userStage)
    ->set('http_group', 'pascln') // Группа пользователя, которую вы указали в $userStage
    ->set('deploy_path', '/var/www/vhosts/rocketfirm.net/myproject.rocketfirm.net') // Путь к папке проекта
    ->set('keep_releases', 3);  
```