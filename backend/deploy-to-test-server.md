# Разворачивание сайта на тестовом сервере

## Создание сайта
1. Заходим в [Plesk](https://rocketfirm.net:8443/) сервера rocketfirm.net. Логин и пароль в этой [карточке](https://trello.com/c/hbKBmIj3/45-vultrcom-rocketfirmnet). Нажимаем на **Add Subdomain**   
![pleskrocketfirmnet](./images/pleskrocketfirmnet1.png)

2. В поле *Subdomain name* пишем название вашего проекта.  
В поле *Document root* автоматически подставится путь к вашему проекту, под названием 
**название_проекта.rocketfirm.net**.  
Отмечаем галочку рядом с *Secure the domain with Let's Encrypt*.  
И нажимаем на **OK**.  
![pleskrocketfirmnet](./images/pleskrocketfirmnet2.png)

3. Далее нажимаем на **Files**, который находится слева в меню. Выбираем в списке **rocketfirm.net** и находим там нашу папку проекта. Выберите все файлы и нажмите на **Remove**.  
![pleskrocketfirmnet](./images/pleskrocketfirmnet3.png)

4. Нажимаем на **Websites & Domains**, который находится слева в меню. Выбираем в списке наш сайт **название_проекта.rocketfirm.net** и нажимаем на **Hosting Settings**.
![pleskrocketfirmnet](./images/pleskrocketfirmnet6.png)

1. В поле *Document root* указываем папку, где лежит основной файл `index.php`. У разных фреймворков этот файл хранится в разных папках. В **Yii2** это папка `web`, а для **Laravel** папка `public`.  
Итоговый путь будет у нас таким:
- **Yii2**: `название_проекта.rocketfirm.net/current/web`;
- **Laravel**: `название_проекта.rocketfirm.net/current/public`;  
Отмечаем галочку рядом с *Permanent SEO-safe 301 redirect from HTTP to HTTPS*.  
Выбираем **PHP** версии **7.1.X**.  
Нажимаем на **OK**.  
![pleskrocketfirmnet](./images/pleskrocketfirmnet7.png)

---
## Создание базы данных
1. Нажимаем на **Databases**, который находится слева в меню. Выбираем в списке **rocketfirm.net** и нажимаем на **Add Database**.  
![pleskrocketfirmnet](./images/pleskrocketfirmnet4.png)

2. В поле *Database name* пишем название базы данных. Имя должно начинаться с **db_название_проекта** (подробнее о [стандартах](./naming.md))  
В поле *Related site* выбираем наш сайт.  
В поле *Database user name* пишем имя пользователя. Имя должно быть одинаковым с имененем базы данных (**db_название_проекта**).  
В поле *New password* нажимаем на кнопку Generate, а затем на **Show**, и запишите этот пароль в карточке доступов проекта в **Trello**.
Нажимаем на **OK**.  
![pleskrocketfirmnet](./images/pleskrocketfirmnet5.png)

---
## Деплой
Перед тем как заделоит проект на тестовый сервер, прочитайте пожалуйста гайд по настройке [deployer](./deployer.md).  
Для запуска деплоя используем следующие команды:
- **Yii2**: `dep deploy stage`;
- **Laravel**: `dep deploy staging`;

> ***Важно!***  
> При первом деплое, закоментте строку в файле `deploy.php`, где запускается задача запуска  миграции (при первом деплое обычно еще конфигурации базы данных еще не настроены, так как  файл `.env` еще не создался).
> 
> ### Yii2
> Файл `deploy.php` 
> ```php
>
> task('deploy:run_migrations', function () {
>    // run('cd {{release_path}} && ./vendor/bin/bridge-install'); // Закомментим эту строку
> })->desc('Run migrations');
> ```
>
> ### Laravel
> Файл `deploy.php` 
> ```php
>
> // after('success', 'artisan:migrate'); // Закомментим эту строку
> ```
>
> После того как вы настроили конфигурации базы данных, откомментим ту стороку, которую мы  закомментили выше и запускаем деплой еще раз. 