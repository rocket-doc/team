# Командное руководство

## Код

* [Стиль кода](code/style.md)
    * [Пример](code/style-example.md)
* [Качество кода](code/quality.md)
* [Тестирование](code/test.md)
* [Документирование](code/doc.md)
* Код ревью
    * [Введение](code/review-intro.md)
    * [Основное](code/review-main.md)
    * [Чеклист](code/review-checklist.md)
    * [Советы](code/review-tips.md)
* [To Do](code/todo.md)

## Backend

* [Стандарты именования](backend/naming.md)
* [Настройка окружения разработчика](backend/development-environment.md)
* [Настройка деплоя (Deployer)](backend/deployer.md)
* [Разворачивание сайта на тестовом сервере](backend/deploy-to-test-server.md)
* [Рекомендации по Yii2](backend/yii2.md)
* [Рекомендации по Laravel](backend/laravel.md)
