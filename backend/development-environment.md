# Настройка окружения разработчика

Данная инструкция для систем **macOS**

## Brew
Выполните следующую команду в терминале:
```shell
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
![brew](./images/brew1.png) 
После запуска команды, вам предложит нажать на "Return" (Enter) чтобы продолжить. Нажимаем "Return" (Enter). После установки перезагружаем терминал.
![brew](./images/brew2.png) 

Подробнее о [Brew](https://brew.sh/index_ru).

---
## MySQL
Выполните следующую команду в терминале:
```shell
brew install mysql@5.7
```
![mysql](./images/mysql1.png)
После установки MySQL, выполняем эти команды поочередно, после чего перезагружаем терминал:
```shell
echo 'export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"' >> ~/.bash_profile
```
```shell
brew services start mysql@5.7
```
Для того, чтобы убедиться правильно ли установлен у нас MySQL выполняем в терминале команду `mysql -uroot`, и внутри него можно например выполнить команду `SHOW DATABASES;`
![mysql](./images/mysql2.png)
Для MySQL клиента, мы обычно используем [Sequel Pro](https://sequelpro.com/download). Скачиваем последнюю версию и устанавливаем его. После запуска данной программы вы увидите вот такое окно:
![mysql](./images/mysql3.png)

Заполняем данные как на скриншоте ниже. Нажимаем на кнопку **Добавить в закладки** и **Соединить**.
![mysql](./images/mysql4.png)

---
## PHP
Выполните следующую команду в терминале:
```shell
brew install php@7.1
```
![php](./images/php1.png)
После установки PHP, выполняем эти команды поочередно, после чего перезагружаем терминал:
```shell
echo 'export PATH="/usr/local/opt/php@7.1/bin:$PATH"' >> ~/.bash_profile
```
```shell
echo 'export PATH="/usr/local/opt/php@7.1/sbin:$PATH"' >> ~/.bash_profile
```
Далее выполняем команду, после чего перезагружаем терминал:
```shell
brew unlink php@7.1 && brew link php@7.1 --force
```
Выполните команду `which php`, чтобы убедиться используем ли мы PHP, которую мы установили через brew (по умолчанию с macOS идет встроенный PHP, не путайте с ним).
![php](./images/php2.png)
Если после команды `which php` вы получили строку вроде этой `/usr/bin/php`, это значит что вы все еще используете PHP, который идет по умолчанию с macOS. Выполните команду эти команды поочередно, после чего перезагружаем терминал:
```shell
brew link php@7.1 --force
```
```shell
echo 'export PATH="/usr/local/opt/php@7.1/bin:$PATH"' >> ~/.bash_profile
```
```shell
echo 'export PATH="/usr/local/opt/php@7.1/sbin:$PATH"' >> ~/.bash_profile
```

---
## Composer
Выполните следующую команду в терминале:
```shell
brew install composer
```
После установки перезапустите терминал и выполните команду `composer -V`, чтобы убедиться что он правильно установлен.
![composer](./images/composer1.png)

---
## Deployer
Выполните следующую команду в терминале:
```shell
cd ~/ && curl -LO https://deployer.org/deployer.phar && mv deployer.phar /usr/local/bin/dep && chmod +x /usr/local/bin/dep
```
Выполните команду `dep -V`, чтобы убедиться что оно правильно установлен.
![deployer](./images/deployer1.png)
Другие способы [установки](https://deployer.org/docs/installation.html).

---
## Valet
Выполните следующую команду в терминале:
```shell
composer global require laravel/valet
```
После установки, выполняем следующую команду, после чего перезагружаем терминал.
```shell
export PATH="$PATH:$HOME/.composer/vendor/bin"
```
Выполните команду `valet -V`, чтобы убедиться что оно правильно установлен.
![valet](./images/valet1.png)
Далее выполняем команду:
```shell
valet install
```
После установки выполните команду `ping foobar.test`, вы должны увидеть результаты как на скриншоте ниже.
![valet](./images/valet2.png)
Устанавливаем драйвер для Valet, чтобы работать с проектами на Yii2. Выполните следующую команду:
Далее выполняем команду:
```shell
cd ~/ && git clone https://gist.github.com/349112047c91ea8441256db4dc390b9c.git && cd 349112047c91ea8441256db4dc390b9c && mv Yii2ValetDriver.php ~/.config/valet/Drivers/ && rm -rf ~/349112047c91ea8441256db4dc390b9c && cd ~/ && valet restart
```

Следующее что мы должны сделать, это перейти к папке, где у нас лежат все проекты. Если у вас все еще нету такой папки, то выполните эти команды поочередно:
```shell
mkdir ~/sites
```
```shell
cd ~/sites/
```
Итак, в нашем папке проектов (*~/sites/*) выполните команду `valet park`. Затем выполните команду `valet path`, чтобы убедиться, что вы правильно настроили папку.
![valet](./images/valet3.png)

Подробнее о [Valet](https://laravel.com/docs/5.8/valet).

---
## Redis
Выполните следующие команды в терминале поочередно:
```shell
brew install redis
```
```shell
brew services start redis
```
![redis](./images/redis1.png)
Выполните команду `redis-cli`, и внутри Redis’а выполните команду `INFO`, чтобы убедиться что оно правильно установлен (вы должны получить информацию как на скриншоте выше).
Чтобы выйти из Redis’а выполните команду `EXIT`.