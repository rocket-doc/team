# Стандарты именования

**snake_case** - post_category, is_active, position   
**spinal-case** - post-category, is-active, position  
**camelCase** - postCategory, isActive, position  
**PascalCase** - PostCategory, IsActive, Position 

## Общие

Что | Правило | Стиль написания | Правильно | Не правильно
------------ | ------------ | ------------- | ------------- | -------------
Контроллер | ед. ч. и Controller | PascalCase | ArticleController, CategoryController | ArticlesController, CategoriesController
Маршруты | мн. ч. | spinal-case | articles/1 | article/1
Модель | ед. ч. | PascalCase | User, City | Users, Cities
Свойство модели | - | snake_case | $model->created_at | $model->createdAt
Методы | - | camelCase | getAll | get_all
Метод в тесте | - | camelCase | testGuestCannotSeeArticle | test_guest_cannot_see_article
Переменные | - | camelCase | $articlesWithAuthor | $articles_with_author
Объекты | описательное, мн. ч. | camelCase | $activeUsers = User::active()->get() | $active, $data
Объект | описательное, ед. ч. | camelCase | $activeUser = User::active()->first() | $users, $obj
Интерфейс | прилагательное или существительное | PascalCase | Authenticatable | AuthenticationInterface, IAuthentication
Трейт | прилагательное | PascalCase | Notifiable | NotificationTrait

---
## База данных

Что | Правило | Стиль написания | Правильно | Не правильно
------------ | ------------ | ------------- | ------------- | -------------
Название базы данных | должен начинаться с префикса **db_** и название проекта | spinal-case | db_project, db_e-commerce | database1, project, DB_PROJECT
Имя пользователя | должен быть одинаковым с названием базы данных | spinal-case | db_project, db_e-commerce | database1, project, DB_PROJECT
Таблица | мн. ч. | snake_case | article_comments, categories | article_comment, articleComments, category
Промежуточная таблица | имена моделей в алфавитном порядке в ед. ч. | snake_case | article_user | user_article, articles_users
Колонки в таблице | без имени модели | snake_case | meta_title, is_active | MetaTitle; article_meta_title, isActive
Колонка первичного ключа | - | - | id | custom_id
Колонка внешнего ключа в таблице | название_таблицы в ед. ч. и _id | snake_case | article_id | ArticleId, id_article, articles_id
Имя внешнего ключа в таблице | fk-название_таблицы-название_колонки | spinal-case | fk-posts-author_id | author_id, AuthorId, fk-author_id
Имя индекса в таблице | idx-название_таблицы-название_колонки | spinal-case | idx-posts-is_active | is_active, IsActive, idx-is_active
Имя составного индекса в таблице | idx-название_таблицы-название_колонки-название_колонки-т.д. | spinal-case | idx-posts-is_active-position | is_active-position, IsActivePosition,        idx-is_active-idx-position
Имя уникального индекса в таблице | uq-название_таблицы-название_колонки | spinal-case | uq-posts-slug | slug, Slug, uq-slug
Имя уникального составного индекса в таблице | uq-название_таблицы-название_колонки-название_колонки-т.д. | spinal-case | uq-posts-post_id-lang | post_id-lang, PostIdLang, uq-post_id-uq-lang

---
## Миграции

Что | Правило | Стиль написания |  Правильно | Не правильно
------------ | ------------ | ------------- | ------------- | -------------
Создание таблицы | create_название_таблицы_table | snake_case | 2017_01_01_000000_create_articles_table | 2017_01_01_000000_articles
Удаление таблицы | drop_название_таблицы_table | snake_case | 2017_01_01_000000_drop_articles_table | 2017_01_01_000000_articles
Добавление колонки | add_название_колонки_column_to_название_таблицы_table | snake_case | 2017_01_01_000000_add_position_column_to_articles_table | 2017_01_01_000000_add_position
Изменение колонки | alter_название_колонки_column_at_название_таблицы_table | snake_case | 2017_01_01_000000_alter_position_column_at_articles_table | 2017_01_01_000000_alter_position
Удаление колонки | drop_название_колонки_column_from_название_таблицы_table | snake_case | 2017_01_01_000000_drop_position_column_from_articles_table | 2017_01_01_000000_drop_position